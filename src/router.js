import Vue from 'vue';
import Router from 'vue-router';
import Home from './pages/Join.vue';
import Host from './pages/Host.vue';
import Question from './pages/Question.vue';
import Winner from './pages/Winner.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/question',
      name: 'question',
      component: Question,
    },
    {
      path: '/host',
      name: 'host',
      component: Host,
    },
    {
      path: '/winner',
      name: 'winner',
      component: Winner,
    },
  ],
});
